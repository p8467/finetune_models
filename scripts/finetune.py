#!/usr/bin/env python3

import dataclasses
import logging
from statistics import mode
import torch
import os
import sys
import glob
import pickle
import shutil
from dataclasses import dataclass, field
from typing import Any, Dict, Optional, Tuple
from pynvml import *
from datetime import datetime as dt

import numpy as np
import pandas as pd

from transformers import AutoConfig, AutoModelForSequenceClassification, AutoModelForTokenClassification, AutoTokenizer, EvalPrediction
from transformers import (
    HfArgumentParser,
    TrainingArguments,
    Trainer,
    TrainerControl,
    TrainerState,
    TrainerCallback,
    set_seed
) 
from transformers.file_utils import cached_property, is_torch_available, torch_required
from utils.data.prosa_dataset import ProsaDataset, ProsaDataTrainingArguments
from utils.data.prosa_processors import ProsaDataProcessor, prosa_output_modes
from utils.data.prosa_metrics import prosa_compute_metrics, align_predictions

logger = logging.getLogger(__name__)

def default_logdir(model_name) -> str:
    """
    Same default as PyTorch
    """
    import socket
    from datetime import datetime

    current_time = datetime.now().strftime("%b%d_%H:%M:%S")
    return os.path.join("runs", model_name + "_" + current_time)

@dataclass
class ModelArguments:
    """
    Arguments pertaining to which model/config/tokenizer we are going to fine-tune from.
    """
    output_dir: Optional[str] = field(
        default=None,
        metadata={"help": "Path to pretrained model or model identifier from huggingface.co/models"}
    )
    model_name_or_path: Optional[str] = field(
        default=None,
        metadata={"help": "Path to pretrained model or model identifier from huggingface.co/models"}
    )
    config_name: Optional[str] = field(
        default=None, metadata={"help": "Pretrained config name or path if not the same as model_name"}
    )
    tokenizer_name: Optional[str] = field(
        default=None, metadata={"help": "Pretrained tokenizer name or path if not the same as model_name"}
    )
    cache_dir: Optional[str] = field(
        default=None, metadata={"help": "Where do you want to store the pretrained models downloaded from s3"}
    )
    cuda: Optional[str] = field(
        default=None, metadata={"help": "Which gpu will be used for finetune"}
    )
    c: Optional[str] = field(
        default=None, metadata={"help": "Which gpu will be used for finetune"}
    )
    batch_size: Optional[int] = field(
        default=None, metadata={"help": "short parameter to set per_gpu_train_batch_size and per_gpu_eval_batch_size"}
    ) 
    b: Optional[int] = field(
        default=None, metadata={"help": "short parameter to set per_gpu_train_batch_size and per_gpu_eval_batch_size"}
    ) 
    epoch: Optional[int] = field(
        default=None, metadata={"help": "short parameter to set num_train_epochs"}
    ) 
    e: Optional[int] = field(
        default=None, metadata={"help": "short parameter to set epoch"}
    ) 
    layer: Optional[str] = field(
        default=None, metadata={"help": "short parameter to set number of layers: 12, 24, 24S"}
    ) 
    l: Optional[str] = field(
        default='12', metadata={"help": "short parameter to set layer"}
    ) 
    tf: bool = field(
        default=False, metadata={"help": "Use --tf in command if you wanna use previous tensorflow bert pretrained models"}
    ) 
    load_output: bool = field(
        default=False, metadata={"help": "Whether to load weights, config and vocabs from output dir"}
    ) 
    load_last_ckpt: bool = field(
        default=False, metadata={"help": "Whether to load weights, config and vocabs from last checkpoint in output dir"}
    ) 
    wrong_only: bool = field(
        default=False, metadata={"help": "Whether to write only wrong predictions to file"}
    )
    lstm: bool = field(
        default=False, metadata={"help": "Whether to add lstm as final layer"}
    )
    crf: bool = field(
        default=False, metadata={"help": "Whether to add crf as final layer"}
    )
    h: bool = field(
        default=False, metadata={"help": "Whether to use hyperparameter optimization during training. it will make training excessively longer"}
    )
    acc: bool = field(
        default=False, metadata={"help": "Whether to use eval accuracy to use as best score comparison during training"}
    )
    do_train: bool = field(
        default=False, metadata={"help": "Whether to run eval best checkpoint on the dev set."}
    )
    do_eval: bool = field(
        default=False, metadata={"help": "Whether to run eval best checkpoint on the dev set."}
    )
    do_eval_best: bool = field(
        default=False, metadata={"help": "Whether to run eval best checkpoint on the dev set."}
    )
    do_predict: bool = field(
        default=False, metadata={"help": "Whether to run eval best checkpoint on the dev set."}
    )
    overwrite_output_dir: bool = field(
        default=True, metadata={"help": "Whether to run eval best checkpoint on the dev set."}
    )
    learning_rate: float = field(
        default=2e-5, metadata={"help": "Whether to run eval best checkpoint on the dev set."}
    )
    logging_steps: Optional[int] = field(
        default=None, metadata={"help": "Whether to run eval best checkpoint on the dev set."}
    )
    save_steps: Optional[int] = field(
        default=None, metadata={"help": "Whether to run eval best checkpoint on the dev set."}
    )
    run_name: str = field(
        default=None, metadata={"help": "Whether to run eval best checkpoint on the dev set."}
    )

class BestCheckpointCallback(TrainerCallback):

    def __init__(self):
        super().__init__()
        self.current_best = -1

    def on_evaluate(self, args: TrainingArguments, state: TrainerState, control: TrainerControl, **kwargs):
        metric = args.metric_for_best_model
        score = kwargs.get('metrics').get(metric)

        if not os.path.isdir(os.path.join(args.output_dir, 'reports')):
            os.mkdir(os.path.join(args.output_dir, 'reports'))

        eval_result = state.log_history[len(state.log_history)-1]
        eval_result.update(state.log_history[len(state.log_history)-1])

        output_eval_file = os.path.join(
            os.path.join(args.output_dir, 'reports'), "eval_results_last.csv"
        )
        df = pd.DataFrame(eval_result, index=[0])
        df.to_csv(output_eval_file, index=None)

        if score > self.current_best:
            self.current_best = max(self.current_best, score)
            logger.info('New max result in output_dir, with {} score {}.'.format(metric, self.current_best))

            best_checkpoint_dir = os.path.join(args.output_dir)
            model = kwargs.get('model')
            model.save_pretrained(save_directory=best_checkpoint_dir)

            output_eval_file = os.path.join(
                os.path.join(args.output_dir, 'reports'), "eval_results_best.csv"
            )
            df = pd.DataFrame(eval_result, index=[0])
            df.to_csv(output_eval_file, index=None)

        best_ckpt = state.best_model_checkpoint
        
        print(f'Best Score {self.current_best}')
        print(f'Best ckpt {best_ckpt}')

def finetune(model_args, data_args):
    # Short args assignment
    if data_args.m:
        data_args.model_type = data_args.m
    if data_args.t:
        data_args.task_name = data_args.t.lower()
    if data_args.s:
        data_args.max_seq_length = data_args.s
    if data_args.pd:
        data_args.pretrain_data = data_args.pd
    if model_args.c:
        model_args.cuda = model_args.c
    if model_args.b:
        model_args.batch_size = model_args.b
    if model_args.e:
        model_args.epoch = model_args.e
    if model_args.l:
        model_args.layer = model_args.l
    if model_args.acc:
        eval_metrics = 'eval_accuracy'
    else:
        eval_metrics = 'eval_f1'
    if not model_args.batch_size:
        model_args.batch_size = 16
    if not model_args.epoch:
        model_args.epoch = 3
    if model_args.do_eval_best:
        # We need eval result in output dir for eval best
        model_args.do_eval = True
    if not model_args.do_train and not model_args.load_last_ckpt:
        # if we are not training, then it means we have to load model, config and vocab from output_dir
        model_args.load_output = True
    # if not training_args.do_train: # disable this condition to simplify command
    model_args.overwrite_output_dir = True

    if (not model_args.do_train and 
    not model_args.do_eval and
    not model_args.do_eval_best and
    not model_args.do_predict):
        logging.info("do_train is False, do_eval is False, do_eval_best is False, do_predict is False too. WHAT DO YOU WANT TO DO THEN???")
        return

    # Pretraining_data initialization 
    elif not data_args.pretrain_data:
        data_args.pretrain_data = 'old'

    srv = '/srv/data1/text/miftah/huggingface'
    framework = 'tf' if model_args.tf else 'lm'
    model_id = f'{data_args.model_type}_{framework}_{data_args.pretrain_data}_L{model_args.layer}_S{data_args.max_seq_length}' 
    o = f'{data_args.model_type}/{model_id}'
    a = f'{srv}/{o}'

    path = os.getcwd()
    fndir = path.split('/')[-1]

    if not model_args.output_dir:
        model_args.output_dir = o
    else:
        model_args.output_dir = os.path.join(model_args.output_dir, o)
    model_args.logging_dir = default_logdir(model_id)

    if not model_args.run_name:
        model_args.run_name = model_id
    
    # Convert pd old to en for certain cases
    if data_args.pretrain_data == 'old':
        # prp_en are architectures failed to finetune in doc_pair_cls while using pretrain_data old 
        prp_en = ['roberta', 'albert', 'camembert'] 
        # all_en are architectures that mostly perform better while using pretrain_data en
        all_en = ['xlm','flaubert','bart','longformer']
        if (data_args.task_name == 'doc_pair_cls' and data_args.model_type in prp_en) or data_args.model_type in all_en:
            logger.info(f"USING PRETRAIN_DATA 'OLD' IS BAD FOR THIS TASK, AUTOMATICALLY SET IT TO 'EN'")
            data_args.pretrain_data = 'en' 

    # Check if model with choosen pretrain_data is available
    enl_collection = ["bart", "longformer",  "flaubert", "camembert", "bert", "roberta", "albert", "xlnet"]
    en_collection = enl_collection + ["distilbert", 'flaubert']
    old_collection = enl_collection[3:] + ["electra", "distilbert", 'xlm-roberta']
    mul_collection = ["xlm", "xlm-roberta", "bert", "distilbert"]
    if data_args.pretrain_data == 'enl': # and data_args.model_type not in enl_collection:
        # raise ValueError(f"currently available enl pretrain_data is {enl_collection}")
        raise ValueError(f"for storage efficiency, currently all enl model had been deleted")
    elif data_args.pretrain_data == 'en' and data_args.model_type not in en_collection:
        raise ValueError(f"currently available en pretrain_data is {en_collection}")
    elif data_args.pretrain_data == 'old' and data_args.model_type not in old_collection:
        raise ValueError(f"currently available old pretrain_data is {old_collection}")
    elif data_args.pretrain_data == 'mul' and data_args.model_type not in mul_collection:
        raise ValueError(f"currently available mul pretrain_data is {mul_collection}")

    # Model_name_or_path initialization
    resume_from_checkpoint = False
    combination_finetune = False
    if model_args.model_name_or_path:
        combination_finetune = 'transformers' in model_args.model_name_or_path
    if not model_args.model_name_or_path or combination_finetune:
        if os.path.exists(model_args.output_dir):
            listdir = os.listdir(model_args.output_dir)
            if listdir:
                resume_from_checkpoint = True
                if model_args.load_output:
                    model_args.model_name_or_path = model_args.output_dir
                    model_args.config_name = model_args.output_dir
                    model_args.tokenizer_name = model_args.output_dir
                    logger.info(f"MODEL_ARGS.LOAD_OUTPUT = {model_args.load_output}. Loading weights, config and tokenizer from {model_args.output_dir}")
                else:
                    last = 0 
                    max_d = ''
                    for d in listdir:
                        if 'checkpoint-' in d:
                            no = int(d.split('-')[1])
                            if no > last:
                                max_d = d
                                last = no
                            # last = max(last, no)
                    if last > 0:
                        # ckpt = f"{o}/checkpoint-{last}"
                        ckpt = f"{model_args.output_dir}/{max_d}"
                        if combination_finetune:
                            model_args.model_name_or_path += '/' + max_d
                            ckpt = model_args.model_name_or_path
                        if not model_args.model_name_or_path:
                            model_args.model_name_or_path = ckpt
                        if not model_args.config_name:
                            model_args.config_name = ckpt
                        if not model_args.tokenizer_name:
                            model_args.tokenizer_name = a 
                        logger.info(f"FOUND CHEKPOINT DIR IN OUTPUT. Loading weights and config from {ckpt}")
                if model_args.do_train and not model_args.overwrite_output_dir:
                    raise ValueError(
                        f"Output directory ({model_args.output_dir}) already exists and is not empty. Use --overwrite_output_dir to overcome."
                    )

        if not model_args.model_name_or_path:
            model_args.model_name_or_path = a
        if not model_args.config_name:
            model_args.config_name = a
        if not model_args.tokenizer_name:
            model_args.tokenizer_name = a
    
    logger.info(f"MODEL_ARGS.MODEL_NAME_OR_PATH {model_args.model_name_or_path}")

    tokenizer = AutoTokenizer.from_pretrained(
        model_args.tokenizer_name if model_args.tokenizer_name else model_args.model_name_or_path,
        cache_dir=model_args.cache_dir, config = model_args.config_name,
        problem_type='multi_label_classification' if data_args.task_name=='doc_multi_cls' else None
    )

    # Get datasets
    dt = ProsaDataset
    train_dataset = dt(data_args, tokenizer=tokenizer, fnames=data_args.trains, set_type='train') if model_args.do_train else None
    eval_dataset = dt(data_args, tokenizer=tokenizer, fnames=data_args.devs, set_type='dev') if ( model_args.do_eval or model_args.do_eval_best) else None
    test_dataset = dt(data_args, tokenizer=tokenizer, fnames=data_args.tests, set_type='test') if model_args.do_predict else None

    # Set logging steps and save steps
    if not model_args.logging_steps:
        dataset = None
        if model_args.do_train:
            dataset = train_dataset
        elif model_args.do_eval_best or model_args.do_eval:
            dataset = eval_dataset
        elif model_args.do_predict:
            dataset = test_dataset
        if not dataset:
            raise ValueError(f'DATASET IS NONE. MAKE SURE DATASET HAVE SOME DATA IN IT')
        step = int(len(dataset.features) / model_args.batch_size)
        model_args.logging_steps = step
        model_args.save_steps = step

    # Reset task_name for auto
    if data_args.task_name == 'auto':
        # We call get_task from here instead of processors.prosa if we load dataset from cache
        if not ProsaDataProcessor.task_name: 
            ProsaDataProcessor.get_task(data_args.data_dir)
        data_args.task_name = ProsaDataProcessor.task_name

    # Get label
    labels = dt.label_list
    num_labels = len(labels)
    id2label: Dict[int, str] = {i: label for i, label in enumerate(labels)}
    label2id: Dict[str, int] = {label: i for i, label in enumerate(labels)}

    try:
        output_mode = prosa_output_modes[data_args.task_name]
    except KeyError:
        raise ValueError("Task not found: %s" % (data_args.task_name))

    # Load pretrained model and tokenizer
    #
    # Distributed training:
    # The .from_pretrained methods guarantee that only one local process can concurrently
    # download model & vocab.

    config = None
    try:
        config = AutoConfig.from_pretrained(
            model_args.config_name if model_args.config_name else model_args.model_name_or_path,
            num_labels=num_labels,
            id2label=id2label,
            label2id=label2id,
            finetuning_task=data_args.task_name,
            use_lstm=model_args.lstm,
            use_crf=model_args.crf,
            cache_dir=model_args.cache_dir,
            max_seq_length=data_args.max_seq_length,
            problem_type='multi_label_classification' if data_args.task_name=='doc_multi_cls' else None
        )
    except:
        logger.info(f"MODEL_ARGS.MODEL_NAME_OR_PATH {model_args.model_name_or_path}")
        # pass


    def _compute_metrics(p: EvalPrediction) -> Dict:
        preds = p.predictions # init for output_mode 'sequence'
        if output_mode == "classification":
            preds = np.argmax(p.predictions, axis=1)
        elif output_mode == "regression":
            preds = np.squeeze(p.predictions)
        return prosa_compute_metrics(data_args.task_name, preds, p.label_ids, id2label)
    """
    mt = {'doc_cls':AutoModelForSequenceClassification,
          'doc_pair_cls':AutoModelForSequenceClassification,
          'doc_multi_cls':AutoModelForSequenceClassification,
          'doc_pair_multi_cls':AutoModelForSequenceClassification,
          'token_cls':AutoModelForTokenClassification
    }
    """

    mt = AutoModelForTokenClassification if data_args.task_name == 'token_cls' else AutoModelForSequenceClassification
    model = None
    trainer = None
    training_args = TrainingArguments(
        output_dir=model_args.output_dir,
        overwrite_output_dir=True,
        do_train=model_args.do_train,
        do_eval=model_args.do_eval,
        do_predict=model_args.do_predict,
        evaluation_strategy='epoch' if model_args.do_eval else 'no',
        per_device_train_batch_size=model_args.batch_size,
        per_device_eval_batch_size=model_args.batch_size,
        learning_rate=model_args.learning_rate,
        num_train_epochs=model_args.epoch,
        logging_dir=model_args.logging_dir,
        logging_steps=model_args.logging_steps,
        logging_strategy='epoch',
        save_strategy='epoch',
        save_steps=model_args.save_steps,
        save_total_limit=1,
        load_best_model_at_end=True if model_args.do_eval_best else False,
        metric_for_best_model=eval_metrics
    )

    # Set seed
    set_seed(training_args.seed)

    # Select the emptiest gpu
    os.environ["CUDA_VISIBLE_DEVICES"] = model_args.cuda
    training_args._n_gpu=1
    device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
    torch.cuda.set_device(device)


    logger.warning(
        "Process rank: %s, device: %s, n_gpu: %s, distributed training: %s, 16-bits training: %s",
        training_args.local_rank,
        training_args.device,
        training_args.n_gpu,
        bool(training_args.local_rank != -1),
        training_args.fp16,
    )
    logger.info("Training/evaluation parameters %s", training_args)

    if model_args.do_train or model_args.do_eval or model_args.do_predict:

        # model = mt[data_args.task_name].from_pretrained(
        model = mt.from_pretrained(
            model_args.model_name_or_path,
            from_tf=bool(".ckpt" in model_args.model_name_or_path),
            config=config,
            cache_dir=model_args.cache_dir,
        )
        if not training_args.no_cuda:
            model = model.cuda()

        # Initialize our Trainer
        trainer = Trainer(
            model=model,
            args=training_args,
            train_dataset=train_dataset,
            eval_dataset=eval_dataset,
            compute_metrics=_compute_metrics,
            callbacks=[BestCheckpointCallback] if model_args.do_eval_best else []
        )

    # Training
    if model_args.do_train:
        trainer.train(resume_from_checkpoint=resume_from_checkpoint)
        # For convenience, we also re-save the tokenizer to the same directory,
        # so that you can share your model easily on huggingface.co/models =)
        if trainer.is_world_process_zero():
            tokenizer.save_pretrained(model_args.output_dir)
        if not model_args.do_eval_best:
            model.save_pretrained(save_directory=model_args.output_dir)
    
    if model_args.do_eval or model_args.do_predict:
        model = mt.from_pretrained(
            model_args.output_dir,
            from_tf=bool(".ckpt" in model_args.model_name_or_path),
            config=config,
            cache_dir=model_args.cache_dir,
        )
        trainer = Trainer(
            model=model,
            args=training_args,
            train_dataset=train_dataset,
            eval_dataset=eval_dataset,
            compute_metrics=_compute_metrics
        )
        if model_args.do_eval:
            trainer.evaluate(eval_dataset)
        if model_args.do_predict:
            predictions, label_ids, metrics = trainer.predict(test_dataset) 
            output_test_file = os.path.join(training_args.output_dir, "test_results.")

            # Dont write seqeval.metrics.classification_report in csv
            if metrics:
                key = 'eval_report'
                if key in metrics:
                    report = metrics.pop(key)
                    with open(output_test_file + key, 'w') as w:
                        w.write(report)

            # Save in csv format
            dic = {k:[round(v*100,2)] for k,v in metrics.items() if isinstance(v, float)}
            ps = pd.DataFrame(dic)
            ps.to_csv(output_test_file + 'csv', index=False)

            # Save in txt format
            with open(output_test_file + 'txt', "w") as writer:
                for key, value in metrics.items():
                    logger.info("  %s = %s", key, value)
                    writer.write("%s = %s\n" % (key, value))

            # Write model predictions to file
            if data_args.task_name == 'token_cls':
                output_test_predictions_file = os.path.join(training_args.output_dir, "test_predictions.txt")
                with open(output_test_predictions_file, "w") as writer:
                    preds_list, _ = align_predictions(predictions, label_ids, id2label)
                    test_file = os.path.join(data_args.data_dir, "test.") 
                    if os.path.isfile(test_file + 'txt'):
                        with open(test_file + 'txt', "r") as f:
                            example_id = 0
                            writer.write("word\tlabel\tprediction\n")
                            for line in f:
                                if line.startswith("-DOCSTART-") or line == "" or line == "\n":
                                    writer.write(line)
                                    if not preds_list[example_id]:
                                        example_id += 1
                                elif preds_list[example_id]:
                                    output_line = line.strip() + "\t" + preds_list[example_id].pop(0) + "\n"
                                    writer.write(output_line)
                                # else:
                                #     logger.warning(f"Maximum sequence length exceeded {example_id}: No prediction for '{line.split()[0]}'.")
                    elif os.path.isfile(test_file + 'pkl'):
                        with open(test_file + 'pkl', 'rb') as f:
                            d = pickle.load(f)
                        text_list = d['text_a']
                        label_list = d['label']
                        writer.write("word\tlabel\tprediction\n")
                        for text, label, prediction in zip(text_list, label_list, preds_list):
                            for t, l, p in zip(text.split(), label, prediction):
                                output_line = f'{t}\t{l}\t{p}\n'
                                writer.write(output_line)
                            writer.write('\n')

            elif data_args.task_name in ['doc_cls', 'doc_pair_cls']: 
                # Read data
                all_text_a = []
                all_text_b = []
                all_label = []
                for fname in data_args.tests:
                    data = f"{data_args.data_dir}/{fname}"
                    ps = None
                    if os.path.isfile(f"{data}.csv"):
                        ps = pd.read_csv(f"{data}.csv")
                    elif os.path.isfile(f"{data}.tsv"):
                        ps = pd.read_csv(f"{data}.tsv", sep='\t')
                    else: continue
                    for _, row in ps.iterrows():
                        text_a = str(row['text_a'])
                        label = str(row['label'])
                        field = [text_a, label]

                        # Handle doc_pair_cls
                        text_b = None
                        if data_args.task_name == 'doc_pair_cls':
                            text_b = str(row['text_b'])
                            field.append(text_b)

                        # Make sure there is no empty or nan field
                        if any(isinstance(x, float) or not x for x in field):
                            logger.warning(f"THERE IS AN EMPTY FIELD IN ROW {row}")
                            continue

                        # Append to list
                        all_text_a.append(text_a)
                        all_text_b.append(text_b)
                        all_label.append(label)

                # Create dataframe
                dic = {'text_a':all_text_a, 'label':all_label}
                if data_args.task_name == 'doc_pair_cls':
                    dic['text_b'] = all_text_b
                ps = pd.DataFrame(dic) 

                # Combine dataframe with prediction
                scores = np.exp(predictions) / np.exp(predictions).sum(-1, keepdims=True)
                confidence = [item.max().item() for item in scores]
                preds = np.argmax(predictions, axis=1)
                assert len(ps['text_a']) == len(ps['label']) == len(preds), f"DIFFERENT LENGTH BETWEEN DATA AND PREDICTION {ps} {len(preds)}"
                preds_list = [id2label[l] for l in preds] 
                pd_pred = pd.DataFrame({'prediction':preds_list, 'confidence':confidence})
                ps = pd.concat([ps, pd_pred], axis=1)

                # If model_args.wrong_only is True, filter out correct prediction 
                if model_args.wrong_only:
                    wrong_idx = ps['prediction'] != ps['label']
                    ps = ps[wrong_idx]

                # Write to csv
                output_test_predictions_file = os.path.join(training_args.output_dir, "test_predictions.csv")
                ps.to_csv(output_test_predictions_file, index=False)

            elif data_args.task_name == 'doc_multi_cls':
                threshold = 0.5
                preds = 1/(1 + np.exp(-predictions)) > threshold
                all_text_a = []
                all_label = []
                dc = None
                for fname in data_args.tests:
                    data = f"{data_args.data_dir}/{fname}.pkl"
                    if os.path.isfile(f"{data}"):
                        f = open(data, 'rb')
                        dc = pickle.load(f)
                        all_text_a += dc['text_a']
                        all_label += dc['label']
                hpreds = []
                hlabels = []
                for pred, label in zip(preds, all_label):
                    hpred = [dt.label_list[i] for i,x in enumerate(pred) if x]
                    hpreds.append(hpred)
                    hlabel = [dt.label_list[i] for i,x in enumerate(label) if x]
                    hlabels.append(hlabel)
                dc['label'] = hlabels
                dc['prediction'] = hpreds
                ps = pd.DataFrame.from_dict(dc)
                output_test_predictions_file = os.path.join(training_args.output_dir, "test_predictions.csv")
                ps.to_csv(output_test_predictions_file, index=False)

def main():
    # See all possible arguments in src/transformers/training_args.py
    # or by passing the --help flag to this script.
    # We now keep distinct sets of args, for a cleaner separation of concerns.

    parser = HfArgumentParser((ModelArguments, ProsaDataTrainingArguments))

    if len(sys.argv) == 2 and sys.argv[1].endswith(".json"):
        # If we pass only one argument to the script and it's the path to a json file,
        # let's parse it to get our arguments.
        model_args, data_args = parser.parse_json_file(json_file=os.path.abspath(sys.argv[1]))
    else:
        model_args, data_args = parser.parse_args_into_dataclasses()

    # Setup logging
    logging.basicConfig(
        format="%(asctime)s - %(levelname)s - %(name)s -   %(message)s",
        datefmt="%m/%d/%Y %H:%M:%S",
        level=logging.INFO,
    )

    success = False 
    # while not success:
    #     torch.cuda.empty_cache()
    #     try:
    start = dt.now()
    finetune(model_args, data_args)
    logger.info(f"FINETUNE IS FINISHED. TOOK {dt.now() - start} HOURS")
    success = True
    #     except Exception as e:
    #         logger.info(f"FAILED FINETUNING. ERROR : {e}")
    #         is_memory = 'memory' in str(e).lower()
    #         if model_args.b == 2 or not is_memory:
    #             break
    #         model_args.b -= 2
    #         logger.info(f"BATCH REDUCED TO {model_args.b}") 

if __name__ == "__main__":
    main()
