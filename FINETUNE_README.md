# Transformers

Transformers is a Python library for pretraining and finetuning transformers models.

## Logs

Detailed logs during exploration, pretraining, and experiment can be found in this [gsheet](https://docs.google.com/spreadsheets/d/1pQjUZFa3HptoK0uW04WJu6-FzbOuq9VoaZgB3k6muJU/edit?usp=sharing)

## Environment

```bash
source /home/miftahul.mahfuzh/vnboost/bin/activate
```

## Usage

```bash
finetune --m camembert --pd en --c 0
```

## Quick Parameters

```bash
  --m, Required[str], default=None, alias=model_type
                        pass a model type from the 
                        list: distilbert, albert, camembert, xlm-roberta,
                        bart, roberta, bert, openai-gpt, gpt2, transfo-xl,
                        xlnet, flaubert, xlm, ctrl, electra
  --pd, Optional[str], default=lm, alias=pretrain_data
                        Choose which pretrain data
                        used for pretrained model. [lm, en, enl, mix5050]
  --l, Optional[int], default=12, alias=layer
                        short parameter to set number of layers: 12 or 24
  --t, Optional[str], default=None, alias=task_name
                        The name of the task to train on: doc_cls,
                        doc_pair_cls, token_cls
  --c, Optional[str], default=None, alias=cuda
                        Which gpu will be used for finetune
  --b, Optional[int], default=None, alias=batch
                        Short parameter to set per_device_train_batch_size and
                        per_device_eval_batch_size 
  --e, Optional[int], default=None, alias=epoch
                        Short parameter to set num_train_epochs 
  --nt, Optional[bool], default=False, alias=--no-do-train
                        Use it if you do not want to finetune the train set.
  --neb, Optional[bool], default=False, alias=--no-do-eval-best
                        Use it if you do not want to eval_best the dev set.
  --np, Optional[bool], default=False, alias=--no-do-predict
                        Use it if you do not want to predict the test set.
  --w, Optional[str], default=None, alias=--wandb
                        Short parameter to set wandb_project 
```

## All Parameters

```bash
-h, --help - show this help message and exit
  --model_type MODEL_TYPE, Required[str], default=None
                        pass a model type from the 
                        list: distilbert, albert, camembert, xlm-roberta,
                        bart, roberta, bert, openai-gpt, gpt2, transfo-xl,
                        xlnet, flaubert, xlm, ctrl, electra
  --pretrain_data PRETRAIN_DATA, Optional[str], default=lm
                        Pretrain data used for pretrained model. lm, en,
                        mix5050
  --layer LAYER, Optional[int], default=12
                        short parameter to set number of layers: 12 or 24
  --task_name TASK_NAME, Optional[str], default=None
                        The name of the task to train on: doc_cls,
                        doc_pair_cls, token_cls
  --cuda CUDA, Optional[str], default=None       
                        Which gpu will be used to finetune
  --batch BATCH, Optional[int], default=None         
                        short parameter to set per_gpu_train_batch_size and
                        per_gpu_eval_batch_size 
  --epoch EPOCH, Optional[int], default=None
                        short parameter to set num_train_epochs 
  --wandb_project WANDB_PROJECT, Optional[str], default=None
                        Use this parameter to set wandb project
  --no-do_train, Optional[bool]
                        Use it if you do not want to finetune train data.
  --no-do_eval, Optional[bool]
                        Use it if you do not want to eval dev data.
  --no-do_eval_best, Optional[bool]
                        Use it if you do not want to eval_best dev data.
  --no-do_predict, Optional[bool]
                        Use it if you do not want to predict test data.
  --no_cuda, Optional[bool], default=False        
                        Avoid using CUDA even if it is available
  --data_dir DATA_DIR, Optional[str], default=data
                        The input data dir. Should contain the .tsv files (or
                        other data files) for the task.
  --output_dir OUTPUT_DIR, Optional[str], default=.
                        The output directory where the model predictions and
                        checkpoints will be written.  
  --load_output LOAD_OUTPUT, Optional[bool], default=False
                        Whether to load weights, config and vocabs in output
                        dir. useful when training is finished 
  --load_last_ckpt LOAD_LAST_CKPT, Optional[bool], default=False
                        Will load weights, config in last checkpoint dir and
                        load vocabs in pretrained dir
                        useful when training is killed before finished
  --wrong_only WRONG_ONLY, Optional[bool], default=False
                        Write only wrong predictions to file during predict
  --overwrite_output_dir, Optional[bool], default=False
                        Overwrite the content of the output directory.Use this
                        to continue training if output_dir points to a
                        checkpoint directory
  --overwrite_cache, Optional[bool], default=False    
                        Overwrite the cached training and evaluation sets
  --no-evaluate_during_training, Optional[bool]
                        Use it if you do not want to run evaluation during 
  --num_train_epochs NUM_TRAIN_EPOCHS, Optional[int], default=3
                        Total number of training epochs to perform.
                        training at each logging step.
  --per_device_train_batch_size PER_GPU_TRAIN_BATCH_SIZE, Optional[int], default=32
                        Batch size per GPU/CPU for training. 
                        Bigger batch size requires bigger memory
  --per_device_eval_batch_size PER_GPU_EVAL_BATCH_SIZE, Optional[int], default=32
                        Batch size per GPU/CPU for evaluation.
                        Bigger batch size requires bigger memory
                        performing a backward/update pass.
  --learning_rate LEARNING_RATE, Optional[float], default=5e-5
                        The initial learning rate for Adam.
  --logging_steps LOGGING_STEPS, Optional[int], default=5000
                        Log every X updates steps.
  --save_steps SAVE_STEPS, Optional[int], default=5000
                        Save checkpoint every X updates steps.
  --save_total_limit SAVE_TOTAL_LIMIT, Optional[int], default=2
                        Limit the total amount of checkpoints. Deletes the
                        older checkpoints in the output_dir. Default is
                        unlimited checkpoints
  --gradient_accumulation_steps GRADIENT_ACCUMULATION_STEPS, Optional[int], default=1
                        Number of updates steps to accumulate before
  --weight_decay WEIGHT_DECAY, Optional[float], default=0.0
                        Weight decay if we apply some.
  --adam_epsilon ADAM_EPSILON, Optional[float], default=1e-8
                        Epsilon for Adam optimizer.
  --max_grad_norm MAX_GRAD_NORM, Optional[float], default=1.0
                        Max gradient norm.
  --max_steps MAX_STEPS, Optional[int], default=-1
                        If > 0: set total number of training steps to perform.
                        Override num_train_epochs.
  --warmup_steps WARMUP_STEPS, Optional[int], default=0
                        Linear warmup over warmup_steps.
  --logging_dir LOGGING_DIR, Optional[str], default=None
                        Tensorboard log dir.
  --logging_first_step, Optional[bool], default=False  
                        Log and eval the first global_step
  --model_name_or_path MODEL_NAME_OR_PATH, Optional[str], default=None
                        Path to pretrained model or model identifier from
                        huggingface.co/models
  --config_name CONFIG_NAME, Optional[str], default=None
                        Pretrained config name or path if not the same as
                        model_name
  --tokenizer_name TOKENIZER_NAME, Optional[str], default=None
                        Pretrained tokenizer name or path if not the same as
                        model_name
  --cache_dir CACHE_DIR, Optional[str], default=None
                        Where do you want to store the pretrained models
                        downloaded from s3
  --max_seq_length MAX_SEQ_LENGTH, Optional[int], default=256
                        The maximum total input sequence length after
                        tokenization. Sequences longer than this will be
                        truncated, sequences shorter will be padded.
``` 
