# coding=utf-8
""" Prosa processors and helpers """

import csv
from dataclasses import dataclass
import logging
import os
import pandas as pd
import glob
import pickle
import json
import dataclasses
from enum import Enum
from typing import List, Optional, Union

from transformers.file_utils import is_tf_available
from transformers.tokenization_utils import PreTrainedTokenizer
from transformers.models.auto.tokenization_auto import AutoTokenizer
from transformers.data.processors.utils import DataProcessor, InputExample, InputFeatures


if is_tf_available():
    import tensorflow as tf

logger = logging.getLogger(__name__)

@dataclass
class InputExampleTk:
    """
    A single training/test example for simple sequence classification.

    Args:
        guid: Unique id for the example.
        text_a: string. The untokenized text of the first sequence. For single
            sequence tasks, only this sequence must be specified.
        text_b: (Optional) string. The untokenized text of the second sequence.
            Only must be specified for sequence pair tasks.
        label: (Optional) string. The label of the example. This should be
            specified for train and dev examples, but not for test examples.
    """

    guid: str
    text_a: List[str] = None
    text_b: List[str] = None
    labels: Optional[List[str]] = None

    def to_json_string(self):
        """Serializes this instance to a JSON string."""
        return json.dumps(dataclasses.asdict(self), indent=2) + "\n"

@dataclass(frozen=True)
class InputFeaturesTk:
    """
    A single set of features of data.
    Property names are the same names as the corresponding inputs to a model.

    Args:
        input_ids: Indices of input sequence tokens in the vocabulary.
        attention_mask: Mask to avoid performing attention on padding token indices.
            Mask values selected in ``[0, 1]``:
            Usually  ``1`` for tokens that are NOT MASKED, ``0`` for MASKED (padded) tokens.
        token_type_ids: (Optional) Segment token indices to indicate first and second
            portions of the inputs. Only some models use them.
        label: (Optional) Label corresponding to the input. Int for classification problems,
            float for regression problems.
    """

    input_ids: List[int]
    attention_mask: Optional[List[int]] = None
    token_type_ids: Optional[List[int]] = None
    label_ids: Optional[List[int]] = None

    def to_json_string(self):
        """Serializes this instance to a JSON string."""
        return json.dumps(dataclasses.asdict(self)) + "\n"



def convert_examples_to_features_sq(
    examples: Union[List[InputExample], "tf.data.Dataset"],
    tokenizer: PreTrainedTokenizer,
    max_length: Optional[int] = None,
    task=None,
    label_list=None,
    output_mode=None,
):
    """
    Loads a data file into a list of ``InputFeatures``

    Args:
        examples: List of ``InputExamples`` or ``tf.data.Dataset`` containing the examples.
        tokenizer: Instance of a tokenizer that will tokenize the examples
        max_length: Maximum example length. Defaults to the tokenizer's max_len
        task: One of prosa task: doc_cls, doc_pair_cls, token_cls
        label_list: List of labels. Can be obtained from the processor using the ``processor.get_labels()`` method
        output_mode: String indicating the output mode. Either ``regression`` or ``classification``

    Returns:
        If the ``examples`` input is a ``tf.data.Dataset``, will return a ``tf.data.Dataset``
        containing the task-specific features. If the input is a list of ``InputExamples``, will return
        a list of task-specific ``InputFeatures`` which can be fed to the model.

    """
    if is_tf_available() and isinstance(examples, tf.data.Dataset):
        if task is None:
            raise ValueError("When calling prosa_convert_examples_to_features from TF, the task parameter is required.")
        return _tf_convert_examples_to_features_sq(examples, tokenizer, max_length=max_length, task=task)
    return _convert_examples_to_features_sq(
        examples, tokenizer, max_length=max_length, task=task, label_list=label_list, output_mode=output_mode
    )


if is_tf_available():

    def _tf_convert_examples_to_features_sq(
        examples: tf.data.Dataset, tokenizer: PreTrainedTokenizer, task=str, max_length: Optional[int] = None,
    ) -> tf.data.Dataset:
        """
        Returns:
            A ``tf.data.Dataset`` containing the task-specific features.

        """
        processor = prosa_processors[task]()
        examples = [processor.tfds_map(processor.get_example_from_tensor_dict(example)) for example in examples]
        features = prosa_convert_examples_to_features(examples, tokenizer, max_length=max_length, task=task)

        def gen():
            for ex in features:
                yield (
                    {
                        "input_ids": ex.input_ids,
                        "attention_mask": ex.attention_mask,
                        "token_type_ids": ex.token_type_ids,
                    },
                    ex.label,
                )

        return tf.data.Dataset.from_generator(
            gen,
            ({"input_ids": tf.int32, "attention_mask": tf.int32, "token_type_ids": tf.int32}, tf.int64),
            (
                {
                    "input_ids": tf.TensorShape([None]),
                    "attention_mask": tf.TensorShape([None]),
                    "token_type_ids": tf.TensorShape([None]),
                },
                tf.TensorShape([]),
            ),
        )


def _convert_examples_to_features_sq(
    examples: None, # List[InputExample],
    tokenizer: PreTrainedTokenizer,
    max_length: Optional[int] = None,
    task=None,
    label_list=None,
    output_mode=None,
):
    if max_length is None:
        max_length = tokenizer.max_len

    if task is not None:
        processor = prosa_processors[task]()
        if label_list is None:
            label_list = processor.get_labels()
            logger.info("Using label list %s for task %s" % (label_list, task))
        if output_mode is None:
            output_mode = prosa_output_modes[task]
            logger.info("Using output mode %s for task %s" % (output_mode, task))

    label_map = {label: i for i, label in enumerate(label_list)}

    def label_from_example(example: InputExample) -> Union[int, float]:
        if output_mode == "classification":
            return label_map[example.label]
        elif output_mode == "regression":
            return float(example.label)
        elif output_mode == "classification_multi":
            return example.labels # we use labels because this is InputExampleTk
        raise KeyError(output_mode)

    labels = [label_from_example(example) for example in examples]

    batch_encoding = None
    '''
    if output_mode == "classification_multi":
        batch_encoding = tokenizer.batch_encode_plus(
            [(example.text_a, example.text_b) for example in examples], max_length=max_length, pad_to_max_length=True,
        )
    else:
    '''
    batch_encoding = tokenizer.batch_encode_plus(
        [[example.text_a, example.text_b]  if example.text_b else [example.text_a, ""] for example in examples], max_length=max_length, pad_to_max_length=True,
    )

    features = []
    for i in range(len(examples)):
        inputs = {k: batch_encoding[k][i] for k in batch_encoding}

        feature = None
        if output_mode == "classification_multi":
            feature = InputFeaturesTk(**inputs, label_ids=labels[i])
        else:
            feature = InputFeatures(**inputs, label=labels[i])
        features.append(feature)

    for i, example in enumerate(examples[:5]):
        logger.info("*** Example ***")
        logger.info("guid: %s" % (example.guid))
        logger.info("features: %s" % features[i])
        if output_mode == "classification_multi":
            labels = examples[i].labels
            logger.info("labels: %s" % labels)
            h_labels = [label_list[i] for i,l in enumerate(labels) if l]
            logger.info(f"text_a: {examples[i].text_a}")
            logger.info(f"labels: {h_labels} -> {labels}")
        else:
            logger.info(f"text_a: {examples[i].text_a}")
            logger.info(f"text_b: {examples[i].text_b}")
            logger.info(f"label: {examples[i].label} -> {labels[i]}")

    return features

def convert_examples_to_features_tk(
    examples: List[InputExampleTk],
    label_list: List[str],
    max_seq_length: int,
    tokenizer: PreTrainedTokenizer,
    cls_token_at_end=False,
    cls_token="[CLS]",
    cls_token_segment_id=1,
    sep_token="[SEP]",
    sep_token_extra=False,
    pad_on_left=False,
    pad_token=0,
    pad_token_segment_id=0,
    pad_token_label_id=-100,
    sequence_a_segment_id=0,
    mask_padding_with_zero=True,
) -> List[InputFeaturesTk]:
    """ Loads a data file into a list of `InputFeatures`
        `cls_token_at_end` define the location of the CLS token:
            - False (Default, BERT/XLM pattern): [CLS] + A + [SEP] + B + [SEP]
            - True (XLNet/GPT pattern): A + [SEP] + B + [SEP] + [CLS]
        `cls_token_segment_id` define the segment id associated to the CLS token (0 for BERT, 2 for XLNet)
    """
    # TODO clean up all this to leverage built-in features of tokenizers

    label_map = {label: i for i, label in enumerate(label_list)}

    features = []
    for (ex_index, example) in enumerate(examples):
        if ex_index % 10_000 == 0:
            logger.info("Writing example %d of %d", ex_index, len(examples))

        tokens = []
        label_ids = []
        for word, label in zip(example.text_a, example.labels):
            word_tokens = tokenizer.tokenize(word)

            # bert-base-multilingual-cased sometimes output "nothing ([]) when calling tokenize with just a space.
            if len(word_tokens) > 0:
                tokens.extend(word_tokens)
                # Use the real label id for the first token of the word, and padding ids for the remaining tokens
                label_ids.extend([label_map[str(label)]] + [pad_token_label_id] * (len(word_tokens) - 1))

        # Account for [CLS] and [SEP] with "- 2" and with "- 3" for RoBERTa.
        special_tokens_count = tokenizer.num_special_tokens_to_add()
        if len(tokens) > max_seq_length - special_tokens_count:
            tokens = tokens[: (max_seq_length - special_tokens_count)]
            label_ids = label_ids[: (max_seq_length - special_tokens_count)]

        # The convention in BERT is:
        # (a) For sequence pairs:
        #  tokens:   [CLS] is this jack ##son ##ville ? [SEP] no it is not . [SEP]
        #  type_ids:   0   0  0    0    0     0       0   0   1  1  1  1   1   1
        # (b) For single sequences:
        #  tokens:   [CLS] the dog is hairy . [SEP]
        #  type_ids:   0   0   0   0  0     0   0
        #
        # Where "type_ids" are used to indicate whether this is the first
        # sequence or the second sequence. The embedding vectors for `type=0` and
        # `type=1` were learned during pre-training and are added to the wordpiece
        # embedding vector (and position vector). This is not *strictly* necessary
        # since the [SEP] token unambiguously separates the sequences, but it makes
        # it easier for the model to learn the concept of sequences.
        #
        # For classification tasks, the first vector (corresponding to [CLS]) is
        # used as as the "sentence vector". Note that this only makes sense because
        # the entire model is fine-tuned.
        tokens += [sep_token]
        label_ids += [pad_token_label_id]
        if sep_token_extra:
            # roberta uses an extra separator b/w pairs of sentences
            tokens += [sep_token]
            label_ids += [pad_token_label_id]
        segment_ids = [sequence_a_segment_id] * len(tokens)

        if cls_token_at_end:
            tokens += [cls_token]
            label_ids += [pad_token_label_id]
            segment_ids += [cls_token_segment_id]
        else:
            tokens = [cls_token] + tokens
            label_ids = [pad_token_label_id] + label_ids
            segment_ids = [cls_token_segment_id] + segment_ids

        input_ids = tokenizer.convert_tokens_to_ids(tokens)

        # The mask has 1 for real tokens and 0 for padding tokens. Only real
        # tokens are attended to.
        input_mask = [1 if mask_padding_with_zero else 0] * len(input_ids)

        # Zero-pad up to the sequence length.
        padding_length = max_seq_length - len(input_ids)
        if pad_on_left:
            input_ids = ([pad_token] * padding_length) + input_ids
            input_mask = ([0 if mask_padding_with_zero else 1] * padding_length) + input_mask
            segment_ids = ([pad_token_segment_id] * padding_length) + segment_ids
            label_ids = ([pad_token_label_id] * padding_length) + label_ids
        else:
            input_ids += [pad_token] * padding_length
            input_mask += [0 if mask_padding_with_zero else 1] * padding_length
            segment_ids += [pad_token_segment_id] * padding_length
            label_ids += [pad_token_label_id] * padding_length

        input_ids = input_ids[:max_seq_length]
        input_mask = input_mask[:max_seq_length]
        segment_ids = segment_ids[:max_seq_length]
        label_ids = label_ids[:max_seq_length]

        assert len(input_ids) == max_seq_length, f'{len(input_ids)} {max_seq_length}'
        assert len(input_mask) == max_seq_length, f'{len(input_mask)} {max_seq_length}'
        assert len(segment_ids) == max_seq_length, f'{len(segment_ids)} {max_seq_length}'
        assert len(label_ids) == max_seq_length, f'{len(label_ids)} {max_seq_length}'

        if ex_index < 5:
            logger.info("*** Example ***")
            logger.info("guid: %s", example.guid)
            logger.info("tokens: %s", " ".join([str(x) for x in tokens]))
            logger.info("input_ids: %s", " ".join([str(x) for x in input_ids]))
            logger.info("input_mask: %s", " ".join([str(x) for x in input_mask]))
            logger.info("segment_ids: %s", " ".join([str(x) for x in segment_ids]))
            logger.info("label_ids: %s", " ".join([str(x) for x in label_ids]))

        if "token_type_ids" not in tokenizer.model_input_names:
            segment_ids = None

        features.append(
            InputFeaturesTk(
                input_ids=input_ids, attention_mask=input_mask, token_type_ids=segment_ids, label_ids=label_ids
            )
        )
    return features

class OutputMode(Enum):
    classification = "classification"
    regression = "regression"


class ProsaDataProcessor(DataProcessor):
    def get_task(data_dir):
        for fname in glob.glob(f"{data_dir}/*"):
            if any(x in fname for x in ['.csv','.tsv']):
                ps = {}
                if '.csv' in fname:
                    ps = pd.read_csv(fname)
                elif '.tsv' in fname:
                    ps = pd.read_csv(fname, sep='\t')
                if 'text_b' in ps:
                    DataProcessor.task_name = 'doc_pair_cls'
                elif 'text_a' in ps:
                    DataProcessor.task_name = 'doc_cls'
            elif '.txt' in fname:
                if not any(x in fname for x in ['train','dev','test']): continue
                DataProcessor.task_name = 'token_cls'
            elif '.pkl' in fname:
                f = open(fname, 'rb')
                dc = pickle.load(f)
                text_a_0 = dc['text_a'][0].split()
                label_0 = dc['label'][0]
                text_a_1 = dc['text_a'][1].split()
                label_1 = dc['label'][1]
                if len(text_a_0) == len(label_0) and len(text_a_1) == len(label_1):
                    # There is a possibility for text_a in doc_multi_cls to have the same length as label
                    # If that happens, make sure 1st and 2nd data always have different length between text_a and label
                    DataProcessor.task_name = 'token_cls'
                else:
                    DataProcessor.task_name = 'doc_multi_cls'
            if DataProcessor.task_name:
                return True
        if not DataProcessor.task_name:
            raise ValueError(f"NO MATCHING FILE TYPE FOR ANY TASKS : [csv, tsv, txt, pkl]")

    def get_example_from_tensor_dict(self, tensor_dict):
        """Gets an example from a dict with tensorflow tensors
        Args:
            tensor_dict: Keys and values should match the corresponding Glue
                tensorflow_dataset examples.
        """
        raise NotImplementedError()

    def get_train_examples(self, data_dir):
        """Gets a collection of `InputExample`s for the train set."""
        raise NotImplementedError()

    def get_dev_examples(self, data_dir):
        """Gets a collection of `InputExample`s for the dev set."""
        raise NotImplementedError()

    def get_test_examples(self, data_dir):
        """Gets a collection of `InputExample`s for the test set."""
        raise NotImplementedError()

    def get_examples(self, data_dir, data):
        """Gets a collection of `InputExample`s for data [train, dev, test]."""
        return self._create_examples(data_dir, data)

    def _doc_cls_create_examples(self, data_dir, fnames):
        """Creates examples for the training and dev sets."""
        examples = []
        idx = 1
        for fname in fnames:
            data = f"{data_dir}/{fname}"
            ps = None
            if os.path.isfile(f"{data}.csv"):
                ps = pd.read_csv(f"{data}.csv")
            elif os.path.isfile(f"{data}.tsv"):
                ps = pd.read_csv(f"{data}.tsv", sep='\t')
            else: continue
            for _, row in ps.iterrows():
                text_a = str(row['text_a'])
                label = str(row['label'])
                field = [text_a, label]
                if any(isinstance(x, float) or not x for x in field):
                    logger.warning(f"THERE IS AN EMPTY FIELD IN ROW {row}")
                    continue
                examples.append(InputExample(guid=f"{fname}-{idx}", text_a=text_a, text_b=None, label=label))
                idx += 1
        return examples

    def _doc_multi_cls_create_examples(self, data_dir, fnames):
        examples = []
        idx = 1
        for fname in fnames:
            data = f"{data_dir}/{fname}.pkl"
            if os.path.isfile(f"{data}"):
                f = open(data, 'rb')
                dc = pickle.load(f)
                if 'text_b' in dc:
                    for text_a, text_b, label in zip(dc['text_a'], dc['text_b'], dc['label']):
                        label = [float(l) for l in label]
                        examples.append(InputExampleTk(guid=f"{fname}-{idx}", text_a=text_a, text_b=text_b, labels=label))
                        idx += 1
                else:
                    for text_a, label in zip(dc['text_a'], dc['label']):
                        label = [float(l) for l in label]
                        examples.append(InputExampleTk(guid=f"{fname}-{idx}", text_a=text_a, labels=label))
                        idx += 1

        return examples

    def _doc_pair_cls_create_examples(self, data_dir, fnames):
        """Creates examples for the training and dev sets."""
        examples = []
        idx = 1
        for fname in fnames:
            data = f"{data_dir}/{fname}"
            ps = None
            if os.path.isfile(f"{data}.csv"):
                ps = pd.read_csv(f"{data}.csv")
            elif os.path.isfile(f"{data}.tsv"):
                ps = pd.read_csv(f"{data}.tsv", sep='\t')
            else: continue
            for _, row in ps.iterrows():
                text_a = str(row['text_a'])
                text_b = str(row['text_b'])
                label = str(row['label'])
                field = [text_a, text_b, label]
                if any(isinstance(x, float) or not x for x in field):
                    logger.warning(f"THERE IS AN EMPTY FIELD IN ROW {row}")
                    continue
                examples.append(InputExample(guid=f"{fname}-{idx}", text_a=text_a, text_b=text_b, label=label))
                idx += 1
        return examples

    def _token_cls_pkl_create_examples(self, data_dir, fnames):
        examples = []
        idx = 1
        for fname in fnames:
            data = f"{data_dir}/{fname}.pkl"
            if os.path.isfile(f"{data}"):
                f = open(data, 'rb')
                dc = pickle.load(f)
                for text_a, label in zip(dc['text_a'], dc['label']):
                    text_a = str(text_a).split()
                    examples.append(InputExampleTk(guid=f"{fname}-{idx}", text_a=text_a, labels=label))
                    idx += 1

        return examples

    def _token_cls_txt_create_examples(self, data_dir, fnames):
        examples = []
        idx = 1
        # Read all txt files
        for fname in fnames:
            data = f"{data_dir}/{fname}.txt"
            if os.path.isfile(f"{data}"):
                with open(data, encoding="utf-8") as f:
                    words = []
                    labels = []
                    for line in f:
                        if line.startswith("-DOCSTART-") or line == "" or line == "\n":
                            if words:
                                examples.append(InputExampleTk(guid=f"{fname}-{idx}", text_a=words, labels=labels))
                                idx += 1
                                words = []
                                labels = []
                        else:
                            splits = line.split("\t")
                            words.append(splits[0])
                            if len(splits) > 1:
                                labels.append(splits[-1].replace("\n", ""))
                            else:
                                # Examples could have no label for set_type = "test"
                                labels.append("O")
                    if words:
                        examples.append(InputExampleTk(guid=f"{fname}-{idx}", text_a=words, labels=labels))
        return examples

    @staticmethod
    def get_labels(data_dir):
        """Gets the list of labels for this data set."""
        label_file = os.path.join(data_dir, 'labels.txt')
        with open(label_file) as f:
            res = f.read().splitlines()
            return res

    def tfds_map(self, example):
        """Some tensorflow_datasets datasets are not formatted the same way the GLUE datasets are.
        This method converts examples to the correct format."""
        if len(self.get_labels()) > 1:
            example.label = self.get_labels()[int(example.label)]
        return example

    @classmethod
    def _read_tsv(cls, input_file, quotechar=None):
        """Reads a tab separated value file."""
        with open(input_file, "r", encoding="utf-8-sig") as f:
            return list(csv.reader(f, delimiter="\t", quotechar=quotechar))



class AutoProcessor(ProsaDataProcessor):

    def _create_examples(self, data_dir, fnames):
        ProsaDataProcessor.get_task(data_dir)
        if ProsaDataProcessor.task_name == 'doc_cls':
            return self._doc_cls_create_examples(data_dir, fnames)
        elif ProsaDataProcessor.task_name == 'doc_multi_cls':
            return self._doc_multi_cls_create_examples(data_dir, fnames)
        elif ProsaDataProcessor.task_name == 'doc_pair_cls':
            return self._doc_pair_cls_create_examples(data_dir, fnames)
        elif ProsaDataProcessor.task_name == 'token_cls':
            txt = self._token_cls_txt_create_examples(data_dir, fnames)
            pkl = self._token_cls_pkl_create_examples(data_dir, fnames)
            return txt + pkl
        return []

class DocumentProcessor(ProsaDataProcessor):
    """Processor for the SST-2 data set (prosa version)."""

    def get_example_from_tensor_dict(self, tensor_dict):
        """See base class."""
        return InputExample(
            tensor_dict["idx"].numpy(),
            tensor_dict["sentence"].numpy().decode("utf-8"),
            None,
            str(tensor_dict["label"].numpy()),
        )

    def _create_examples(self, data_dir, fnames):
        return self._doc_cls_create_examples(data_dir, fnames)

class DocumentMultiProcessor(ProsaDataProcessor):
    """Processor for the SST-2 data set (prosa version)."""

    def get_example_from_tensor_dict(self, tensor_dict):
        """See base class."""
        return InputExample(
            tensor_dict["idx"].numpy(),
            tensor_dict["sentence"].numpy().decode("utf-8"),
            None,
            str(tensor_dict["label"].numpy()),
        )

    def _create_examples(self, data_dir, fnames):
        return self._doc_multi_cls_create_examples(data_dir, fnames)

class DocumentPairProcessor(ProsaDataProcessor):
    """Processor for the doc_pair_cls data set (prosa version)."""

    def get_example_from_tensor_dict(self, tensor_dict):
        """See base class."""
        return InputExample(
            tensor_dict["idx"].numpy(),
            tensor_dict["sentence1"].numpy().decode("utf-8"),
            tensor_dict["sentence2"].numpy().decode("utf-8"),
            str(tensor_dict["label"].numpy()),
        )

    def _create_examples(self, data_dir, fnames):
        return self._doc_pair_cls_create_examples(data_dir, fnames)

class TokenProcessor(ProsaDataProcessor):
    """Processor for the token data set ."""

    def get_example_from_tensor_dict(self, tensor_dict):
        """See base class."""
        return InputExample(
            tensor_dict["idx"].numpy(),
            tensor_dict["sentence"].numpy().decode("utf-8"),
            None,
            tensor_dict["label_ids"].numpy()
        )

    def _create_examples(self, data_dir, fnames):
        txt = self._token_cls_txt_create_examples(data_dir, fnames)
        pkl = self._token_cls_pkl_create_examples(data_dir, fnames)
        return txt + pkl

prosa_processors = {
    "auto": AutoProcessor,
    "doc_cls": DocumentProcessor,
    "doc_pair_cls": DocumentPairProcessor,
    "doc_multi_cls": DocumentMultiProcessor,
    "token_cls": TokenProcessor
}

prosa_output_modes = {
    "doc_cls": "classification",
    "doc_pair_cls": "classification",
    "doc_multi_cls": "classification_multi",
    "token_cls": "sequence"
}
