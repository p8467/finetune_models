# coding=utf-8
# Copyright 2018 The Google AI Language Team Authors and The HuggingFace Inc. team.
# Copyright (c) 2018, NVIDIA CORPORATION.  All rights reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from torch import nn
import numpy as np
from transformers.trainer import EvalPrediction
from typing import Dict, List, Optional, Tuple
from seqeval.metrics import accuracy_score as accuracy_score_tk
from seqeval.metrics import f1_score as f1_score_tk
from seqeval.metrics import precision_score as precision_score_tk
from seqeval.metrics import recall_score as recall_score_tk
from seqeval.metrics import classification_report
import math

try:
    from scipy.stats import pearsonr, spearmanr
    from sklearn.metrics import matthews_corrcoef, f1_score, precision_score, recall_score, accuracy_score, roc_curve, auc

    _has_sklearn = True
except (AttributeError, ImportError):
    _has_sklearn = False


def is_sklearn_available():
    return _has_sklearn

def accuracy_thresh(y_pred:np.ndarray, y_true:np.ndarray, thresh:float=0.5, sigmoid:bool=True):
    "Compute accuracy when `y_pred` and `y_true` are the same size."
    if sigmoid: y_pred = 1/(1 + np.exp(-y_pred))
    y_pred = y_pred > thresh
    bool_t = y_pred
    return np.mean(bool_t==y_true, axis=1).sum() / y_pred.shape[0]

def f1_multilabel(y_pred:np.ndarray, y_true:np.ndarray, thresh:float=0.5, beta:float=1, eps:float=1e-9, sigmoid:bool=True):
    "Computes the f_beta between `preds` and `targets`"
    if sigmoid: y_pred = 1/(1 + np.exp(-y_pred))
    y_pred_bool = [[1 if y > thresh else 0 for y in label] for label in y_pred]
    p = precision_score(y_true, y_pred_bool, average='macro')
    r = recall_score(y_true, y_pred_bool, average='macro')
    f1 = f1_score(y_true, y_pred_bool, average='macro')
    return {'precision':p, 'recall':r, 'f1':f1}
'''
def f1_multilabel(y_pred:np.ndarray, y_true:np.ndarray, thresh:float=0.5, beta:float=1, eps:float=1e-9, sigmoid:bool=True):
    "Computes the f_beta between `preds` and `targets`"
    if sigmoid: y_pred = 1/(1 + np.exp(-y_pred))
    y_pred_tmp = y_pred > thresh
    bool_t = y_pred_tmp
    TP = (bool_t * y_true).sum()
    prec = TP / (y_pred.sum() + eps)
    rec = TP / (y_true.sum() + eps)
    res = (prec * rec) / (prec * beta + rec + eps) * (1 + beta)
    p = prec.mean().item()
    r = rec.mean().item()
    f1 = res
    return {'precision':p, 'recall':r, 'f1':f1}
'''
def f1_acc_roc_auc(all_logits, all_labels, label_map):
    # Compute ROC curve and ROC area for each class
    fpr = dict()
    tpr = dict()
    acc = accuracy_thresh(all_logits, all_labels)
    roc_auc = f1_multilabel(all_logits, all_labels)
    roc_auc['accuracy'] = acc

    for i,label in label_map.items():
        fpr[label], tpr[label], _ = roc_curve(all_labels[:, i], all_logits[:, i])
        roc_auc[label] = auc(fpr[label], tpr[label])

        # Compute micro-average ROC curve and ROC area
        fpr["micro"], tpr["micro"], _ = roc_curve(all_labels.ravel(), all_logits.ravel())
        roc_auc["micro"] = auc(fpr["micro"], tpr["micro"])
    return roc_auc

if _has_sklearn:

    def simple_accuracy(preds, labels):
        return (preds == labels).mean()

    def acc_and_f1(preds, labels):
        acc = simple_accuracy(preds, labels)
        f1 = f1_score(y_true=labels, y_pred=preds)
        return {
            "acc": acc,
            "f1": f1,
            "acc_and_f1": (acc + f1) / 2,
        }

    def pearson_and_spearman(preds, labels):
        pearson_corr = pearsonr(preds, labels)[0]
        spearman_corr = spearmanr(preds, labels)[0]
        return {
            "pearson": pearson_corr,
            "spearmanr": spearman_corr,
            "corr": (pearson_corr + spearman_corr) / 2,
        }

    def acc_f1_p_and_r(preds, labels):
        average = 'macro'
        acc = accuracy_score(y_true=labels, y_pred=preds)
        f1 = f1_score(y_true=labels, y_pred=preds, average=average)
        r = recall_score(y_true=labels, y_pred=preds, average=average)
        p = precision_score(y_true=labels, y_pred=preds, average=average)
        return {
            "accuracy": acc,
            "f1": f1,
            "precision":p,
            "recall":r
        }

    def align_predictions(predictions: np.ndarray, label_ids: np.ndarray, label_map: Dict[int, str]) -> Tuple[List[int], List[int]]:
        preds = np.argmax(predictions, axis=2)

        batch_size, seq_len = preds.shape

        out_label_list = [[] for _ in range(batch_size)]
        preds_list = [[] for _ in range(batch_size)]

        for i in range(batch_size):
            for j in range(seq_len):
                if label_ids[i, j] != nn.CrossEntropyLoss().ignore_index:
                    out_label_list[i].append(label_map[label_ids[i][j]])
                    preds_list[i].append(label_map[preds[i][j]])

        return preds_list, out_label_list

    def compute_metrics_tk(p : EvalPrediction, label_map : Dict[int, str]) -> Dict:
        preds_list, out_label_list = align_predictions(p.predictions, p.label_ids, label_map)
        return {
            "precision": precision_score_tk(out_label_list, preds_list, average='macro'),
            "recall": recall_score_tk(out_label_list, preds_list, average='macro'),
            "f1": f1_score_tk(out_label_list, preds_list, average='macro'),
            "accuracy": accuracy_score_tk(out_label_list, preds_list),
            "report": '\n\n' + classification_report(out_label_list, preds_list),
        }

    def prosa_compute_metrics(task_name, preds, labels, label_map=None):
        # label_map : idlabel -> {0:'O', 1:'B', 2:'I'}
        assert len(preds) == len(labels)
        p = EvalPrediction(predictions=preds, label_ids=labels)
        if task_name == "token_cls":
            return compute_metrics_tk(p, label_map)
        elif task_name == "doc_multi_cls":
            return f1_acc_roc_auc(preds, labels, label_map)
        else:
            return acc_f1_p_and_r(preds, labels)
