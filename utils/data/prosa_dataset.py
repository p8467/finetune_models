import logging
import os
import time
from typing import List, Optional
from dataclasses import dataclass, field

import torch
from torch.utils.data.dataset import Dataset
from torch import nn

from transformers.tokenization_utils import PreTrainedTokenizer
from utils.data.prosa_processors import ProsaDataProcessor, prosa_output_modes, prosa_processors
from utils.data.prosa_processors import convert_examples_to_features_tk, convert_examples_to_features_sq
from transformers.data.processors.utils import  InputFeatures
from transformers.models.auto.modeling_auto import MODEL_WITH_LM_HEAD_MAPPING
from transformers.trainer_pt_utils import torch_distributed_zero_first

logger = logging.getLogger(__name__)

MODEL_CONFIG_CLASSES = list(MODEL_WITH_LM_HEAD_MAPPING.keys())
MODEL_TYPES = tuple(conf.model_type for conf in MODEL_CONFIG_CLASSES)

def list_field(default=None, metadata=None):
    return field(default_factory=lambda: default, metadata=metadata)

@dataclass
class ProsaDataTrainingArguments:
    """
    Arguments pertaining to what data we are going to input our model for training and eval.

    Using `HfArgumentParser` we can turn this class
    into argparse arguments to be able to specify them on
    the command line.
    """

    m: str = field(
        metadata={"help": "If training from scratch, pass a model type from the list: " + ", ".join(MODEL_TYPES)},
    )
    pd: str = field(
        default=None,
        metadata={"help": "Short of pretrain_data. Choose which pretrain data used for pretrained model. lm, en, mix5050"}
    )
    t: Optional[str] = field(default='auto', metadata={"help": "The name of the task to train on: " + ", ".join(prosa_processors.keys())})
    model_type: Optional[str] = field(
        default=None,
        metadata={"help": "If training from scratch, pass a model type from the list: " + ", ".join(MODEL_TYPES)},
    )
    pretrain_data: Optional[str] = field(
        default=None,
        metadata={"help": "Pretrain data used for pretrained model. lm, en, mix5050"}
    )
    task_name: Optional[str] = field(default='auto', metadata={"help": "The name of the task to train on: " + ", ".join(prosa_processors.keys())})
    data_dir: str = field(
        default='data',
        metadata={"help": "The input data dir. Should contain the .tsv files (or other data files) for the task."}
    )
    max_seq_length: int = field(
        default=256,
        metadata={
            "help": "The maximum total input sequence length after tokenization. Sequences longer "
            "than this will be truncated, sequences shorter will be padded."
        },
    )
    s: int = field(
        default=256,
        metadata={
            "help": "Alias for max_seq_length. The maximum total input sequence length after tokenization. Sequences longer "
            "than this will be truncated, sequences shorter will be padded."
        },
    )
    overwrite_cache: bool = field(
        default=False, metadata={"help": "Overwrite the cached training and evaluation sets"}
    )
    trains: Optional[List[str]] = list_field(default=['train'], metadata={"help": "List of data trains"})
    devs: Optional[List[str]] = list_field(default=['dev'], metadata={"help": "List of data devs"})
    tests: Optional[List[str]] = list_field(default=['test'], metadata={"help": "List of data tests"})

    def __post_init__(self):
        self.t = self.t.lower()

class ProsaDataset(Dataset):
    args: ProsaDataTrainingArguments
    output_mode: str
    features = None # : List[InputFeatures]
    pad_token_label_id: int = nn.CrossEntropyLoss().ignore_index
    label_list = []

    def __init__(
        self,
        args: ProsaDataTrainingArguments,
        tokenizer: PreTrainedTokenizer,
        set_type: str, # train, dev, test
        fnames: List[str],
        limit_length: Optional[int] = None,
        local_rank=-1
    ):
        self.args = args
        processor = prosa_processors[args.task_name]()
        ProsaDataset.label_list = processor.get_labels(args.data_dir)
        # Load data features from cache or dataset file
        cached_features_file = os.path.join(
            args.data_dir,
            "cache/{}_{}_{}_{}".format(
                set_type, tokenizer.__class__.__name__, str(args.max_seq_length), args.pretrain_data,
            ),
        )
        with torch_distributed_zero_first(local_rank):
            # Make sure only the first process in distributed training processes the dataset,
            # and the others will use the cache.

            if os.path.exists(cached_features_file) and not args.overwrite_cache:
                start = time.time()
                self.features = torch.load(cached_features_file)
                logger.info( f"Loading features from cached file {cached_features_file} [took %.3f s]", time.time() - start)
            else:
                logger.info(f"Creating features from dataset file at {args.data_dir}")
                examples = processor.get_examples(args.data_dir, fnames)
                if args.task_name == 'auto':
                    args.task_name = ProsaDataProcessor.task_name
                self.output_mode = prosa_output_modes[args.task_name]
                if limit_length is not None:
                    examples = examples[:limit_length]
                if args.task_name == 'token_cls':
                    self.features = convert_examples_to_features_tk(
                        examples,
                        self.label_list,
                        args.max_seq_length,
                        tokenizer,
                        cls_token_at_end=bool(self.args.model_type in ["xlnet"]),
                        # xlnet has a cls token at the end
                        cls_token=tokenizer.cls_token,
                        cls_token_segment_id=2 if self.args.model_type in ["xlnet"] else 0,
                        sep_token=tokenizer.sep_token,
                        sep_token_extra=bool(self.args.model_type in ["roberta"]),
                        # roberta uses an extra separator b/w pairs of sentences, cf. github.com/pytorch/fairseq/commit/1684e166e3da03f5b600dbb7855cb98ddfcd0805
                        pad_on_left=bool(tokenizer.padding_side == "left"),
                        pad_token=tokenizer.pad_token_id,
                        pad_token_segment_id=tokenizer.pad_token_type_id,
                        pad_token_label_id=self.pad_token_label_id,
                    )
                else:
                    self.features = convert_examples_to_features_sq(
                        examples,
                        tokenizer,
                        max_length=args.max_seq_length,
                        label_list=self.label_list,
                        output_mode=self.output_mode,
                    )
                if local_rank in [-1, 0]:
                    start = time.time()
                    cached_dir = os.path.join(args.data_dir,'cache')
                    if not os.path.isdir(cached_dir):
                        os.mkdir(cached_dir)
                    torch.save(self.features, cached_features_file)
                    # ^ This seems to take a lot of time so I want to investigate why and how we can improve.
                    logger.info( f"Saving features into cached file %s [took %.3f s]", cached_features_file, time.time() - start)

    def __len__(self):
        return len(self.features)

    def __getitem__(self, i) -> InputFeatures:
        return self.features[i]
