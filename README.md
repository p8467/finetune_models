
<!-- PROJECT LOGO -->
<div align="center">
<p>
  <a href="https://gitlab.com/prosa.ai/platform/template">
    <img src="resources/images/prosa_logo.jpeg" alt="Logo">
  </a>

  <h3 align="center">Transformers</h3>

  <p align="center">
   Transformers
    <br />
    <a href="https://gitlab.com/prosa.ai/platform/template"><strong>Explore the docs »</strong></a>
    <br />
    <br />
    <a href="https://pm.dev.prosa.ai">Report Bug</a>
    ·
    <a href="https://pm.dev.prosa.ai">Request Feature</a>
  </p>
</p>
</div>


<!-- TABLE OF CONTENTS -->
## Table of Contents

* [About the Project](#about-the-project)
  * [Built With](#built-with)
* [Getting Started](#getting-started)
  * [Prerequisites](#prerequisites)
  * [Installation](#installation)
  * [Usage](#usage)
* [Acknowledgements](#acknowledgements)
* [List of Maintainers](#list-of-maintainers)
* [License](#license)



<!-- ABOUT THE PROJECT -->
## About the Project

![Screenshoot][product-screenshot]

This is a repository for Versionless Transformers.<br>

### Built With


<!-- GETTING STARTED -->
# Getting Started

## Prerequisites
Python ^v3.5

## Installation
- `poetry install` or `pip install -e .` (using Poetry is recommended)
- - If using Poetry, activate `poetry shell` or prefix your python commands with `poetry [cmd]`.

## Usage
Before running the project, you must create a new .env file and put it inside the config folder. Use the provided .env.template for easier time setting up one. For more information about available settings, read the CONFIG.md.

### Running as Docker Containers

1. Build the image by running `$docker build -t <image name>:<tag> .`
2. Edit the docker-compose.yaml with the image you want to deploy.
3. Run `$docker-compose up`.


<!-- ACKNOWLEDGEMENTS -->
# Acknowledgements
This project is possible thanks to many third-party and open source libraries listed in pyproject.toml.

# List of Maintainers
* [Annisa Nurul Azhar](mailto:annisa.azhar@prosa.ai)
* [Miftahul Mahfuzh](mailto:miftahul.mahfuzh@prosa.ai)

# License
Copyright (c) 2020, [Prosa.ai](https://prosa.ai).
<!-- MARKDOWN LINKS & IMAGES -->
[product-screenshot]: https://prosa.ai/static/media/25.ff7d539f.png
