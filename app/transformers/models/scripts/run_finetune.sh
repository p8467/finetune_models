while getopts n:c: flag
do
    case "${flag}" in
        n) name=${OPTARG};;
        c) config_path=${OPTARG};;
    esac
done
echo "Start Training...";
echo "name: $name";
echo $CUDA_VISIBLE_DEVICES

python -m app.transformers.models.finetune ${config_path}